import React, { useEffect } from 'react'
import axios from 'axios'
import {
  View,
  ActivityIndicator,
  StyleSheet,
  AsyncStorage
} from 'react-native'

export default function AuthOrApp(props) {

  useEffect(() => {
    const json = await AsyncStorage.getItem('userData')
    const userData = JSON.parse(json) || {}

    if (userData.token) {
      axios.defaults.headers.common['Authorization']
        = `bearer ${userData.token}`
      props.navigation.navigate('Home', userData)
    } else {
      props.navigation.navigate('Auth')
    }
  })

  return (
    <View style={styles.container}>
      <ActivityIndicator size='large' />
    </View>
  )
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black'
  }
})
