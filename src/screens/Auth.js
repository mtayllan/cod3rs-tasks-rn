import React, { useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native'
import axios from 'axios'
import { server, showError } from '../common'
import AuthInput from '../components/AuthInput'
import commonStyles from '../commonStyles'
import backgroundImage from '../../assets/imgs/login.jpg'

export default function Auth(props) {
  const [stageNew, setStageNew] = useState(false);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const signin = async () => {
    try {
      const res = await axios.post(`${server}/signin`, {
        email, password
      })

      axios.defaults.headers.common['Authorization']
        = `bearer ${res.data.token}`
      AsyncStorage.setItem('userData', JSON.stringify(res.data))
      props.navigation.navigate('Home', res.data)
    } catch (err) {
      Alert.alert('Erro', 'Falha no Login!')
      // showError(err)
    }
  }

  const signup = async () => {
    try {
      await axios.post(`${server}/signup`, {
        name, email, password, confirmPassword
      })

      Alert.alert('Sucesso!', 'Usuário cadastrado :)')
      setStageNew(false);
    } catch (err) {
      showError(err)
    }
  }

  const signinOrSignup = () => {
    if (stageNew) {
      signup()
    } else {
      signin()
    }
  }

  const validations = []

  validations.push(email && email.includes('@'))
  validations.push(password && password.length >= 6)

  if(stageNew) {
    validations.push(name && name.trim())
    validations.push(confirmPassword)
    validations.push(password === confirmPassword)
  }

  const validForm = validations.reduce((all, v) => all && v)

  return (
    <ImageBackground source={backgroundImage}
      style={styles.background}>
      <Text style={styles.title}>Tasks</Text>
      <View style={styles.formContainer}>
        <Text style={styles.subtitle}>
          {stageNew ?
            'Crie a sua conta' : 'Informe seus dados'}
        </Text>
        {stageNew &&
          <AuthInput icon='user' placeholder='Nome'
            style={styles.input}
            value={name}
            onChangeText={value => setName(value)} />}
        <AuthInput icon='at' placeholder='E-mail'
          style={styles.input}
          value={email}
          onChangeText={value => setEmail(value)} />
        <AuthInput icon='lock' secureTextEntry={true}
          placeholder='Senha'
          style={styles.input}
          value={password}
          onChangeText={value => setPassword(value)} />
        {stageNew &&
          <AuthInput icon='asterisk'
            secureTextEntry={true} placeholder='Confirmação'
            style={styles.input}
            value={confirmPassword}
            onChangeText={value => setConfirmPassword(value)} />}
        <TouchableOpacity disabled={!validForm}
          onPress={signinOrSignup}>
          <View style={[styles.button, !validForm ? { backgroundColor: '#AAA' } : {}]}>
            <Text style={styles.buttonText}>
              {stageNew ? 'Registrar' : 'Entrar'}</Text>
          </View>
        </TouchableOpacity>
      </View>
      <TouchableOpacity style={{ padding: 10 }}
        onPress={() => setStageNew(sn => !sn)}>
        <Text style={styles.buttonText}>
          {stageNew ? 'Já possui conta?'
            : 'Ainda não possui conta?'}</Text>
      </TouchableOpacity>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 70,
    marginBottom: 10,
  },
  subtitle: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 20,
  },
  formContainer: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 20,
    width: '90%',
  },
  input: {
    marginTop: 10,
    backgroundColor: '#FFF',
  },
  button: {
    backgroundColor: '#080',
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 20
  }
})
