import React, { useState, useEffect } from 'react'
import {
  Modal,
  View,
  Text,
  TextInput,
  DatePickerIOS,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Alert,
  DatePickerAndroid,
  Platform
} from 'react-native'
import moment from 'moment'
import commonStyles from '../commonStyles'

export default function AddTask (props) {
  const [desc, setDesc] = useState('');
  const [date, setDate] = useState(new Date());

  const getInitialState = () => {
    setDesc('');
    setDate(new Date());
  }

  const save = () => {
    if (!desc.trim()) {
      Alert.alert('Dados inválidos', 'Informe uma descrição para a tarefa');
      return;
    }
    props.onSave({ desc, date });
  }

  const handleDateAndroidChanged = () => {
    DatePickerAndroid.open({
      date: date
    }).then(e => {
      if (e.action !== DatePickerAndroid.dismissedAction) {
        const momentDate = moment(date)
        momentDate.date(e.day)
        momentDate.month(e.month)
        momentDate.year(e.year)
        setDate(momentDate.toDate());
      }
    })
  }

  let datePicker = null;
  if (Platform.OS === 'ios') {
    datePicker = <DatePickerIOS mode='date' date={date}
      onDateChange={newDate => setDate(newDate)} />
  } else {
    datePicker = (
      <TouchableOpacity onPress={handleDateAndroidChanged}>
        <Text style={styles.date}>
          {moment(date).format('ddd, D [de] MMMM [de] YYYY')}
        </Text>
      </TouchableOpacity>
    )
  }

  return (
    <Modal onRequestClose={props.onCancel}
      visible={props.isVisible}
      animationType='slide' transparent={true}
      onshow={() => getInitialState()}>
      <TouchableWithoutFeedback onPress={props.onCancel}>
        <View style={styles.offset}></View>
      </TouchableWithoutFeedback>
      <View style={styles.container}>
        <Text style={styles.header}>Nova Tarefa!</Text>
        <TextInput placeholder="Descrição..." style={styles.input}
          onChangeText={value => setDesc(value)}
          value={desc} />
        {datePicker}
        <View style={{
          flexDirection: 'row',
          justifyContent: 'flex-end'
        }}>
          <TouchableOpacity onPress={props.onCancel}>
            <Text style={styles.button}>Cancelar</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={save}>
            <Text style={styles.button}>Salvar</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableWithoutFeedback onPress={props.onCancel}>
        <View style={styles.offset}></View>
      </TouchableWithoutFeedback>
    </Modal>
  )
}

var styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  offset: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  button: {
    margin: 20,
    marginRight: 30,
    color: commonStyles.colors.default,
  },
  header: {
    fontFamily: commonStyles.fontFamily,
    backgroundColor: commonStyles.colors.default,
    color: commonStyles.colors.secondary,
    textAlign: 'center',
    padding: 15,
    fontSize: 15,
  },
  input: {
    fontFamily: commonStyles.fontFamily,
    width: '90%',
    height: 40,
    marginTop: 10,
    marginLeft: 10,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#e3e3e3',
    borderRadius: 6
  },
  date: {
    fontFamily: commonStyles.fontFamily,
    fontSize: 20,
    marginLeft: 10,
    marginTop: 10,
    textAlign: 'center',
  }
})
