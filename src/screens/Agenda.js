import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  Platform
} from 'react-native'
import axios from 'axios'
import moment from 'moment'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles'
import Task from '../components/Task'
import Icon from 'react-native-vector-icons/FontAwesome'
import ActionButton from 'react-native-action-button'
import AddTask from './AddTask'
import { server, showError } from '../common'

import todayImage from '../../assets/imgs/today.jpg'
import tomorrowImage from '../../assets/imgs/tomorrow.jpg'
import weekImage from '../../assets/imgs/week.jpg'
import monthImage from '../../assets/imgs/month.jpg'

export default function Agenda(props) {
  const [tasks, setTasks] = useState([]);
  const [visibleTasks, setVisibleTasks] = useState([]);
  const [showDoneTasks, setShowDoneTasks] = useState(true);
  const [showAddTask, setShowAddTask] = useState(false);


  loadTasks = async () => {
    try {
      const maxDate = moment()
        .add({ days: props.daysAhead })
        .format('YYYY-MM-DD 23:59')
      const res = await axios.get(`${server}/tasks?date=${maxDate}`);
      setTasks(res.data);
      filterTasks();
    } catch (err) {
      showError(err);
    }
  }

  const addTask = async task => {
    try {
      await axios.post(`${server}/tasks`, {
        desc: task.desc,
        estimateAt: task.date
      })
      setShowAddTask(false);
      loadTasks();
    } catch (err) {
      showError(err);
    }
  }

  const deleteTask = async id => {
    try {
      await axios.delete(`${server}/tasks/${id}`)
      await loadTasks()
    } catch (err) {
      showError(err)
    }
  }

  const filterTasks = () => {
    let visibleTemp = null
    if (showDoneTasks) {
      visibleTemp = tasks;
    } else {
      const pending = task => task.doneAt === null
      visibleTemp = tasks.filter(pending)
    }
    setVisibleTasks(visibleTemp);
  }

  const toggleFilter = () => {
    setShowDoneTasks(sdt => !sdt);
    filterTasks();
  }

  useEffect(() => {
    loadTasks();
  },[]);

  const toggleTask = async id => {
    try {
      await axios.put(`${server}/tasks/${id}/toggle`)
      await this.loadTasks()
    } catch (err) {
      showError(err)
    }
  }

  let styleColor = null
  let image = null

  switch (props.daysAhead) {
    case 0:
      styleColor = commonStyles.colors.today
      image = todayImage
      break
    case 1:
      styleColor = commonStyles.colors.tomorrow
      image = tomorrowImage
      break
    case 7:
      styleColor = commonStyles.colors.week
      image = weekImage
      break
    default:
      styleColor = commonStyles.colors.month
      image = monthImage
      break
  }

  return (
    <View style={styles.container}>
      <AddTask isVisible={showAddTask}
        onSave={addTask}
        onCancel={() => setShowAddTask(false)} />
      <ImageBackground source={image}
        style={styles.background}>
        <View style={styles.iconBar}>
          <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
            <Icon name='bars' size={20} color={commonStyles.colors.secondary} />
          </TouchableOpacity>
          <TouchableOpacity onPress={toggleFilter}>
            <Icon name={showDoneTasks ? 'eye' : 'eye-slash'}
              size={20} color={commonStyles.colors.secondary} />
          </TouchableOpacity>
        </View>
        <View style={styles.titleBar}>
          <Text style={styles.title}>{props.title}</Text>
          <Text style={styles.subtitle}>
            {moment().locale('pt-br').format('ddd, D [de] MMMM')}
          </Text>
        </View>
      </ImageBackground>
      <View style={styles.taksContainer}>
        <FlatList data={visibleTasks}
          keyExtractor={item => `${item.id}`}
          renderItem={({ item }) =>
            <Task {...item} onToggleTask={toggleTask}
              onDelete={deleteTask} />} />
      </View>
      <ActionButton buttonColor={styleColor}
        onPress={() => setShowAddTask(true) } />
    </View >
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 3,
  },
  titleBar: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  title: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 50,
    marginLeft: 20,
    marginBottom: 10,
  },
  subtitle: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 20,
    marginLeft: 20,
    marginBottom: 30,
  },
  taksContainer: {
    flex: 7,
  },
  iconBar: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
})
